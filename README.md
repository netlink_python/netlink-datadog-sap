# netlink-datadog-sap

Integration of information retrieved from netlink-sap-monitor to Datadog.

## Changes

### 0.0.3

- additional logging
- sid specific tags

### 0.0.1

- rfc_transactional_calls
- updater_queue