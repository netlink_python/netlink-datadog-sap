from netlink.sap.monitor.lock.lock_entry import lock_entries as _lock_entries
from netlink.datadog.core import GaugeMetric
import datetime

from netlink.logging import logger


def lock_entries(rfc_connection, datadog_queue, tags=None):
    logger.verbose(f'Getting lock entries from {rfc_connection}')
    timestamp = int(datetime.datetime.now().timestamp())
    data = _lock_entries(rfc_connection)
    tags = {f"sap_sid:{rfc_connection.sysid}", f"sap_client:{rfc_connection.client}", "sap_type:abap"}.union(
        tags or set()
    )

    gauge_metric = GaugeMetric(
        "netlink_sap.abap.lock_entries", host=rfc_connection.hostname, source="netlink_sap", tags=tags
    )

    for age in data:
        logger.trace(f'age {age}')
        for lock_table in data[age]:
            logger.trace(f'lock_table {lock_table}')
            for mode in data[age][lock_table]:
                logger.trace(f'mode {mode}')
                for obj in data[age][lock_table][mode]:
                    logger.trace(f'object {obj}')
                    for client in data[age][lock_table][mode][obj]:
                        logger.trace(f'client {client}')
                        for user in data[age][lock_table][mode][obj][client]:
                            logger.trace(f'user {user}')
                            for tcode in data[age][lock_table][mode][obj][client][user]:
                                logger.trace(f'tcode {tcode}')
                                local_tags = {f"sap_age_bracket:{age}",
                                              f"sap_lock_table:{lock_table}",
                                              f"sap_lock_mode:{mode}",
                                              f"sap_lock_object:{obj}",
                                              f"sap_client:{client}",
                                              f"sap_user:{user}",
                                              f"sap_transaction_code:{tcode}"}
                                logger.trace(local_tags)
                                datadog_queue.put(
                                    gauge_metric.metric(
                                        timestamp,
                                        data[age][lock_table][mode][obj][client][user][tcode],
                                        tags=local_tags,
                                    )
                                )
