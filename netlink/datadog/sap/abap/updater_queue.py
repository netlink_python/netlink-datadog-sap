from netlink.sap.monitor import updater_queue as _updater_queue
from netlink.datadog.core import GaugeMetric
import datetime

from netlink.logging import logger


def updater_queue(rfc_connection, datadog_queue, tags=None):
    logger.verbose(f'Getting updater_queue from {rfc_connection}')
    timestamp = int(datetime.datetime.now().timestamp())
    data = _updater_queue(rfc_connection)
    tags = {f"sap_sid:{rfc_connection.sysid}", f"sap_client:{rfc_connection.client}", "sap_type:abap"}.union(
        tags or set()
    )

    gauge_metric = GaugeMetric(
        "netlink_sap.abap.updater_queue", host=rfc_connection.hostname, source="netlink_sap", tags=tags
    )

    for age in data:
        logger.trace(f'age {age}')
        for user in data[age]:
            logger.trace(f'user {user}')
            for report in data[age][user]:
                logger.trace(f'report {report}')
                for tcode in data[age][user][report]:
                    logger.trace(f'tcode {tcode}')
                    for is_error in data[age][user][report][tcode]:
                        logger.trace(f'is_error {is_error}')
                        for status in data[age][user][report][tcode][is_error]:
                            logger.trace(f'is_status {status}')
                            local_tags = {f"sap_age_bracket:{age}",
                                          f"sap_user:{user}",
                                          f"sap_report:{report}",
                                          f"sap_transaction_code:{tcode}",
                                          f"sap_updater_status:{status}"}
                            if is_error:
                                local_tags.add('sap_updater_error')
                            logger.trace(local_tags)
                            logger.trace(status)
                            datadog_queue.put(
                                gauge_metric.metric(
                                    timestamp,
                                    data[age][user][report][tcode][is_error][status],
                                    tags=local_tags,
                                )
                            )
