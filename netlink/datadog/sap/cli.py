import click
from .datadog_sap import DatadogSapMonitor

from netlink.logging import logger


@click.command()
def datadog_sap_cli():
    logger.set_level(logger.VERBOSE)
    logger.show_threading()
    logger.set_file("datadog_sap_monitor.log", log_level=logger.DEBUG)

    monitor = DatadogSapMonitor()
    monitor.start()
